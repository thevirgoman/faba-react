import React from 'react';
import copy from '../copy.svg';
import ReactTooltip from 'react-tooltip';

class CopyClipboard extends React.Component {
  constructor(props) {
    super(props);
  }

  copyToClipboard = (e) => {
    this.textArea.select();
    document.execCommand('copy');
    // This is just personal preference.
    // I prefer to not show the whole text area selected.
    e.target.focus();
  };

  render() {
    return (
      <React.Fragment>
        <ReactTooltip />

        {
          /* Logical shortcut for only displaying the 
            button if the copy command exists */
          document.queryCommandSupported('copy') && (
            <span>
              <img
                data-tip="copy"
                onClick={this.copyToClipboard}
                style={{ width: 20, marginRight: 5 }}
                src={copy}
                alt=""
              />
            </span>
          )
        }
        <form style={{ position: 'absolute', top: '-1000px', left: '-1000px', height: 0, width: 0 }}>
          <textarea
            style={{ position: 'absolute', top: '-1000px', left: '-1000px', height: 0, width: 0 }}
            ref={(textarea) => (this.textArea = textarea)}
            value={this.props.children}
          />
        </form>
      </React.Fragment>
    );
  }
}

export default CopyClipboard;
