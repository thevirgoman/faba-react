import React, { Component } from 'react';

class ComponentToPrint extends Component {
  render() {
    return (
      <React.Fragment>
        <div style={{ direction: 'rtl', padding: 50 }}>
          <h1>اطلاعات تراکنش سازمان {this.props.info.userName}</h1>
          <p>نام: {this.props.info.values.firstName}</p>
          <p>نام خانوادگی: {this.props.info.values.lastName}</p>

          <p>نام پدر: {this.props.info.values.fatherName}</p>

          <p>شماره شناسنامه: {this.props.info.values.idNumber}</p>
          <p>شماره گواهینامه: {this.props.info.values.certNumber}</p>

          <p>
            تراکنش روی بلاکچین:{' '}
            <a
              style={{ color: 'black' }}
              href={`${process.env.REACT_APP_Block_EXPLORER}/${this.props.info.transactionHash}`}
            >
              {this.props.info.transactionHash}
            </a>
          </p>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default ComponentToPrint;
