import React, { Component } from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import { getCurrentUser, signup } from '../services/authService';

class SignUp extends Component {
  constructor() {
    super();
    const user = getCurrentUser();
    if (user) return (window.location = '/panel');
  }

  state = {};

  layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 8,
    },
  };

  tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };

  onFinish = async (values) => {
    try {
      const { data, status } = await signup(values);
      if (status === 201) return (window.location = '/panel');
    } catch (error) {}
  };

  onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  render() {
    return (
      <>
        <div style={{ textAlign: 'center', margin: 50 }}>
          <h2>ثبت نام</h2>
        </div>

        <Form
          {...this.layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={this.onFinish}
          onFinishFailed={this.onFinishFailed}
        >
          <Form.Item
            label="نام کمپانی"
            name="name"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="ایمیل"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="رمز عبور"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...this.tailLayout}>
            <Button type="primary" htmlType="submit">
              ثبت نام
            </Button>
          </Form.Item>
        </Form>
      </>
    );
  }
}

export default SignUp;
