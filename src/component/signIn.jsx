import React, { Component } from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import { signin } from '../services/authService';

class SignIn extends Component {
  state = {};

  layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 8,
    },
  };

  tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };

  onFinish = async (values) => {
    try {
      const { data, status } = await signin(values);
      if (status === 201) return (window.location = '/panel');
    } catch (error) {}
  };

  onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  render() {
    return (
      <>
        <div style={{ textAlign: 'center', margin: 50 }}>
          <h2>ورود</h2>
        </div>

        <Form
          {...this.layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={this.onFinish}
          onFinishFailed={this.onFinishFailed}
        >
          <Form.Item
            label="ایمیل"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="رمز عبور"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...this.tailLayout}>
            <Button type="primary" htmlType="submit">
              ورود
            </Button>
          </Form.Item>
        </Form>
      </>
    );
  }
}

export default SignIn;
