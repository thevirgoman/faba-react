import React, { Component } from 'react';
import {
  Tabs,
  Form,
  Select,
  InputNumber,
  Radio,
  Slider,
  Button,
  Upload,
  Rate,
  Checkbox,
  Row,
  Col,
  Input,
  Modal,
} from 'antd';
import cryptoJs from 'crypto-js';
import { verifyHash } from '../network/tron/election';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import ComponentToPrint from './ComponentToPrint';
import { Switch, Route, Redirect, Link, NavLink } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { UserOutlined, VideoCameraOutlined, UploadOutlined } from '@ant-design/icons';
import { getCurrentUser } from '../services/authService';
import { getCategory } from '../services/categoryService';
import { getSubCategories } from '../services/subCategoryService';

const { Header, Sider, Content } = Layout;
const { TabPane } = Tabs;
const { Option } = Select;

class Verify extends Component {
  constructor() {
    super();
    const user = getCurrentUser();
    if (!user) return (window.location = '/signin');
  }
  state = {
    file: '',
    visible: false,
    stringHash: '',
    blockNumber: 0,
    createdAt: '',
    loading: false,
    allCategories: [],
    allSubCategories: [],
  };

  async componentDidMount() {
    const user = getCurrentUser();
    const { data: allCategories } = await getCategory();
    this.setState({ userName: user.name, allCategories });
  }

  onDepartmantChange = async (value) => {
    this.setState({
      allSubCategories: [],
    });
    const { data, status } = await getSubCategories(value);
    if (status === 200) {
      this.setState({
        allSubCategories: data,
      });
    }
  };

  handleChange = ({ currentTarget: input }) => {
    if (input.files && input.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(input.files[0]);
      reader.onload = (e) => {
        const file = e.target.result.replace(/^data:image\/[a-z]+;base64,/, '');
        this.setState({ file });
      };
    }
  };
  onFullFormFinish = async (values) => {
    values.file = this.state.file;
    this.enterLoading();

    const valuesArray = Object.values(values);
    const result = valuesArray.join('');
    const hash = cryptoJs.SHA256(result);
    const stringHash = hash.toString();

    const transactionObj = await verifyHash(stringHash);

    this.setState({
      loading: false,
      visible: true,
      stringHash: transactionObj.hash,
      blockNumber: parseInt(transactionObj.blockNumbers[0]),
      createdAt: new Date(parseInt(`${transactionObj.createdAt}000`)).toLocaleDateString('fa-IR'),
    });
  };

  onHashFormFinish = async (values) => {
    const stringHash = values.dataHash;
    this.enterLoading();

    const transactionObj = await verifyHash(stringHash);
    console.log(transactionObj);

    this.setState({
      loading: false,
      visible: true,
      stringHash: transactionObj.hash,
      blockNumber: parseInt(transactionObj.blockNumbers[0]),
      createdAt: new Date(parseInt(`${transactionObj.createdAt}000`)).toLocaleDateString('fa-IR'),
    });
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  enterLoading = () => {
    this.setState({ loading: true });
  };

  formItemLayout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 14,
    },
  };
  render() {
    const { loading, allCategories, allSubCategories } = this.state;
    return (
      <>
        <Layout>
          <Sider style={{ minHeight: '100vh' }} trigger={null} collapsible collapsed={this.state.collapsed}>
            <div className="logo" />
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['3']}>
              <Menu.Item key="1" icon={<UserOutlined />}>
                <NavLink to={`/panel`}>تاریخچه</NavLink>
              </Menu.Item>
              <Menu.Item key="2" icon={<UserOutlined />}>
                <NavLink to={`/panel/store`}>ثبت گواهی نامه</NavLink>
              </Menu.Item>
              <Menu.Item key="3" icon={<VideoCameraOutlined />}>
                <NavLink to={`/panel/verify`}>اعتبارسنجی گواهی نامه</NavLink>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Content
              className="site-layout-background"
              style={{
                margin: '24px 16px',
                padding: 24,
                minHeight: 280,
              }}
            >
              <Modal
                title="اطلاعات بلاکچین"
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
              >
                <p>هش اطلاعات شما: {this.state.stringHash}</p>
                <br />
                <p>شماره بلاک: {this.state.blockNumber}</p>
                <br />
                <p>تاریخ ثبت: {this.state.createdAt}</p>
              </Modal>
              <Tabs defaultActiveKey="1" centered>
                <TabPane tab="اعتبار سنجی از بوسیله هش اطلاعات" key="1">
                  <Form name="validate_other" {...this.formItemLayout} onFinish={this.onHashFormFinish}>
                    <Form.Item
                      label="هش اطلاعات"
                      name="dataHash"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا هش اطلاعات خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      wrapperCol={{
                        span: 12,
                        offset: 6,
                      }}
                    >
                      <Button type="primary" htmlType="submit" loading={loading}>
                        اعتبار سنجی
                      </Button>
                    </Form.Item>
                  </Form>
                </TabPane>
                <TabPane tab="اعتبار سنجی از بوسیله ثبت مجدد اطلاعات" key="2">
                  <Form name="validate_other" {...this.formItemLayout} onFinish={this.onFullFormFinish}>
                    <Form.Item
                      label="نام"
                      name="firstName"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا نام خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      label="نام خانوادگی"
                      name="lastName"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا نام خانوادگی خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      label="نام پدر"
                      name="fatherName"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا نام پدر خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      label="شماره شناسنامه"
                      name="idNumber"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا شماره شناسنامه خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      label="شماره گواهینامه"
                      name="certNumber"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا شماره گواهینامه خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>

                    <Form.Item
                      name="category"
                      label="دپارتمان"
                      rules={[{ required: true, message: 'لطفا نام دپارتمان را انتخاب کنید.' }]}
                    >
                      <Select placeholder="" onChange={this.onDepartmantChange} allowClear>
                        {allCategories ? (
                          allCategories.map((cat) => <Option value={cat.id}>{cat.name}</Option>)
                        ) : (
                          <Option value=""></Option>
                        )}
                      </Select>
                    </Form.Item>

                    <Form.Item
                      name="subCategory"
                      label="کلاس"
                      rules={[{ required: true, message: 'لطفا نام کلاس را انتخاب کنید.' }]}
                    >
                      <Select placeholder="" allowClear>
                        {allCategories ? (
                          allSubCategories.map((cat) => <Option value={cat.id}>{cat.name}</Option>)
                        ) : (
                          <Option value=""></Option>
                        )}
                      </Select>
                    </Form.Item>

                    <Form.Item
                      label="فایل گواهینامه"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا شماره گواهینامه خود را وارد کنید.',
                        },
                      ]}
                    >
                      <input required={true} type="file" onChange={this.handleChange} />
                    </Form.Item>

                    <Form.Item
                      wrapperCol={{
                        span: 12,
                        offset: 6,
                      }}
                    >
                      <Button type="primary" htmlType="submit" loading={loading}>
                        اعتبار سنجی
                      </Button>
                    </Form.Item>
                  </Form>
                </TabPane>
              </Tabs>
            </Content>
          </Layout>
        </Layout>
      </>
    );
  }
}

export default Verify;
