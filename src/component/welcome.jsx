import React, { Component } from 'react';
import { Row, Col } from 'antd';
import { Button, Radio } from 'antd';
import { SaveOutlined, HistoryOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { getCurrentUser, getUserInfo } from '../services/authService';

class Welcome extends Component {
  constructor() {
    super();
    const user = getCurrentUser();
    if (!user) return (window.location = '/signin');
  }
  state = {};

  render() {
    return (
      <div>
        <Row justify="space-around">
          <Col span={4}></Col>
          <Col span={4}>
            <Button type="primary" shape="round" icon={<SaveOutlined />} size="large">
              {' '}
              <Link to="/store">ثبت گواهی نامه</Link>
            </Button>
          </Col>
          <Col span={4}>
            <Button type="primary" shape="round" icon={<HistoryOutlined />} size="large">
              {' '}
              <Link to="/verify">اعتبارسنجی گواهی نامه</Link>
            </Button>
          </Col>
          <Col span={4}></Col>
        </Row>
      </div>
    );
  }
}

export default Welcome;
