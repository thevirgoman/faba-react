import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import { UserOutlined, VideoCameraOutlined, UploadOutlined } from '@ant-design/icons';
import { Switch, Route, Redirect, Link, NavLink } from 'react-router-dom';
import Store from './store';
import Verify from './verify';
import { getCurrentUser } from '../services/authService';

const { Header, Sider, Content } = Layout;

class Panel extends Component {
  constructor() {
    super();
    const user = getCurrentUser();
    if (!user) return (window.location = '/signin');
  }
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <Layout>
        <Sider style={{ minHeight: '100vh' }} trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              <NavLink to={`${this.props.match.url}/store`}>ثبت گواهی نامه</NavLink>
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              <NavLink to={`${this.props.match.url}/verify`}>اعتبارسنجی گواهی نامه</NavLink>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            {this.props.children}
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default Panel;
