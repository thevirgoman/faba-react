import React, { Component } from 'react';
import { Table, Button } from 'antd';
import { Switch, Route, Redirect, Link, NavLink } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { UserOutlined, VideoCameraOutlined, UploadOutlined } from '@ant-design/icons';
import { getCurrentUser } from '../services/authService';
import { getHashes } from '../services/hashService';
import { truncate } from '../utils/helpers';
import CopyClipboard from './CopyClipboard';

const { Header, Sider, Content } = Layout;

class History extends React.Component {
  constructor() {
    super();
    const user = getCurrentUser();
    if (!user) return (window.location = '/signin');
  }
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    items: [],
  };
  async componentDidMount() {
    const { data, response } = await getHashes();
    this.setState({
      items: data,
    });
  }

  columns = [
    {
      title: 'نام',
      dataIndex: 'firstName',
    },
    {
      title: 'نام خانوادگی',
      dataIndex: 'lastName',
    },
    {
      title: 'نام پدر',
      dataIndex: 'fatherName',
    },
    {
      title: 'شماره گواهینامه',
      dataIndex: 'certId',
    },
    {
      title: 'شماره ملی',
      dataIndex: 'nationalId',
    },
    {
      title: 'تراکنش',
      dataIndex: 'transaction',
      render: (text, record) => (
        <a
          href={`${process.env.REACT_APP_Block_EXPLORER}/${text}`}
          target="blank"
          style={{ color: 'initial', textDecoration: 'underline' }}
        >
          {truncate(text)}
        </a>
      ),
    },
    {
      title: 'هش اطلاعات',
      dataIndex: 'hash',
      render: (text, record) => (
        <span>
          {truncate(text)}
          <CopyClipboard>{text}</CopyClipboard>
        </span>
      ),
    },
    {
      title: 'دپارتمان',
      dataIndex: 'departman',
    },
    {
      title: 'کلاس',
      dataIndex: 'class',
    },
  ];

  getData = () => {
    return this.state.items.map((item, el) => {
      return {
        key: item.id,
        transaction: item.transaction,
        departman: item.category.name,
        class: item.subcategory.name,
        hash: item.hash,
        firstName: item.firstName,
        lastName: item.firstName,
        fatherName: item.fatherName,
        certId: item.certId,
        nationalId: item.nationalId,
      };
    });
  };

  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
      });
    }, 1000);
  };

  render() {
    const { loading, selectedRowKeys } = this.state;

    return (
      <Layout>
        <Sider style={{ minHeight: '100vh' }} trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              <NavLink to={`/panel`}>تاریخچه</NavLink>
            </Menu.Item>
            <Menu.Item key="2" icon={<UserOutlined />}>
              <NavLink to={`/panel/store`}>ثبت گواهی نامه</NavLink>
            </Menu.Item>
            <Menu.Item key="3" icon={<VideoCameraOutlined />}>
              <NavLink to={`/panel/verify`}>اعتبارسنجی گواهی نامه</NavLink>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            <div>
              <div style={{ marginBottom: 16 }}></div>
              <Table columns={this.columns} dataSource={this.getData()} />
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default History;
