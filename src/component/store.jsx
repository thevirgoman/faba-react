import React, { Component } from 'react';
import { Form, Button, Input, Modal, Col, Row, Select } from 'antd';
import cryptoJs from 'crypto-js';
import { storeHash } from '../network/tron/election';
import { Page, Text, View, Document, StyleSheet, PDFDownloadLink } from '@react-pdf/renderer';
import { getCurrentUser } from '../services/authService';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import ComponentToPrint from './ComponentToPrint';
import { UserOutlined, VideoCameraOutlined, UploadOutlined } from '@ant-design/icons';
import { Switch, Route, Redirect, Link, NavLink } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { createCategory, getCategory } from '../services/categoryService';
import { createSubCategory, getSubCategories } from '../services/subCategoryService';
import { createHash } from '../services/hashService';
import flashMessage from '../utils/flashMessage';
const { Header, Sider, Content } = Layout;
const { Option } = Select;

class Store extends Component {
  constructor() {
    super();
    const user = getCurrentUser();
    if (!user) return (window.location = '/signin');
  }
  state = {
    file: '',
    visible: false,
    stringHash: '123',
    transactionHash: '123',
    loadings: [],
    loading: false,
    userName: '',
    values: {},
    allCategories: [],
    allSubCategories: [],
  };
  ref = React.createRef();

  async componentDidMount() {
    const user = getCurrentUser();
    const { data: allCategories } = await getCategory();
    this.setState({ userName: user.name, allCategories });
  }

  MyDocument = () => {
    const { userName, stringHash } = this.state;

    return (
      <div>
        <p></p>
      </div>
    );
  };

  handleChange = ({ currentTarget: input }) => {
    if (input.files && input.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(input.files[0]);
      reader.onload = (e) => {
        const file = e.target.result.replace(/^data:image\/[a-z]+;base64,/, '');
        this.setState({ file });
      };
    }
  };

  onFinish = async (values) => {
    values.file = this.state.file;
    this.setState({ values });
    this.enterLoading();

    const valuesArray = Object.values(values);
    const result = valuesArray.join('');
    const hash = cryptoJs.SHA256(result);
    const stringHash = hash.toString();

    if (!window.tronWeb) {
      return alert('جهت استفاده از این پلتفرم کیف پول tronweb را نصب کنید.');
    }

    const address = window.tronWeb.address.fromHex(process.env.REACT_APP_CONTRACT_ADDRESS);
    const contract = await window.tronWeb.contract().at(address);
    const transactionHash = await contract.storeHash(stringHash).send();

    const obj = {
      hash: stringHash,
      transaction: transactionHash,
      category: values.category,
      subCategory: values.subCategory,
      firstName: values.firstName,
      lastName: values.lastName,
      fatherName: values.fatherName,
      nationalId: values.idNumber,
      certId: values.certNumber,
    };

    const { data, status } = await createHash(obj);

    // const transactionHash = await storeHash(stringHash);

    flashMessage('تراکنش شما با موفقیت ثبت شد.');

    this.setState({
      loading: false,
      visible: true,
      stringHash,
      transactionHash,
    });
  };

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  enterLoading = () => {
    this.setState({ loading: true });
  };

  addCategoryOnFinish = async (values) => {
    this.enterLoading();

    const obj = {
      name: values.category,
    };
    const { data, status } = await createCategory(obj);

    if (status === 201) {
      const { data: allCategories } = await getCategory();
      this.setState({ allCategories });
      flashMessage('دپارتمان مورد نظر با موفقیت ثبت شد.');
      // this.formCategoryRef.current.resetFields();
    }

    this.setState({ loading: false });
  };
  addCategoryonFinishFailed = (values) => {};

  addSubCategoryOnFinish = async (values) => {
    this.enterLoading();

    const obj = {
      name: values.subCategory,
      category: values.category,
    };
    const { data, status } = await createSubCategory(obj);

    if (status === 201) {
      flashMessage('کلاس مورد نظر با موفقیت ثبت شد.');
      // this.formSubCategoryRef.current.resetFields();
    }

    this.setState({ loading: false });
  };
  addSubCategoryonFinishFailed = (values) => {};

  onDepartmantChange = async (value) => {
    this.setState({
      allSubCategories: [],
    });
    const { data, status } = await getSubCategories(value);
    if (status === 200) {
      this.setState({
        allSubCategories: data,
      });
    }
  };

  formItemLayout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 18,
    },
  };

  addCategoryLayout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 20 },
  };

  render() {
    const { loading, allCategories, allSubCategories } = this.state;

    return (
      <>
        <Layout>
          <Sider style={{ minHeight: '100vh' }} trigger={null} collapsible collapsed={this.state.collapsed}>
            <div className="logo" />
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['2']}>
              <Menu.Item key="1" icon={<UserOutlined />}>
                <NavLink to={`/panel`}>تاریخچه</NavLink>
              </Menu.Item>
              <Menu.Item key="2" icon={<UserOutlined />}>
                <NavLink to={`/panel/store`}>ثبت گواهی نامه</NavLink>
              </Menu.Item>
              <Menu.Item key="3" icon={<VideoCameraOutlined />}>
                <NavLink to={`/panel/verify`}>اعتبارسنجی گواهی نامه</NavLink>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Content
              className="site-layout-background"
              style={{
                margin: '24px 16px',
                padding: 24,
                minHeight: 280,
              }}
            >
              <Modal
                title="اطلاعات تراکنش"
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
              >
                <div>
                  <ComponentToPrint info={this.state} ref={(el) => (this.componentRef = el)} />
                  <ReactToPrint content={() => this.componentRef}>
                    <PrintContextConsumer>
                      {({ handlePrint }) => <button onClick={handlePrint}>دریافت گواهینامه</button>}
                    </PrintContextConsumer>
                  </ReactToPrint>
                </div>
              </Modal>
              <Row>
                <Col span={8} push={16}>
                  <div style={{ background: 'white', padding: 30, marginBottom: 20 }}>
                    <h4>اضافه کردن دپارتمان</h4>
                    <Form
                      {...this.addCategoryLayout}
                      layout="vertical"
                      ref={this.formCategoryRef}
                      name="basic"
                      initialValues={{ remember: true }}
                      onFinish={this.addCategoryOnFinish}
                      onFinishFailed={this.addCategoryonFinishFailed}
                    >
                      <Form.Item
                        label="دپارتمان"
                        name="category"
                        rules={[{ required: true, message: 'لطفا نام دپارتمان را وارد کنید.' }]}
                      >
                        <Input />
                      </Form.Item>

                      <Form.Item>
                        <Button type="primary" htmlType="submit" loading={loading}>
                          ثبت
                        </Button>
                      </Form.Item>
                    </Form>
                  </div>
                  <div style={{ background: 'white', padding: 30, marginBottom: 20 }}>
                    <h4>اضافه کردن کلاس</h4>
                    <Form
                      {...this.addCategoryLayout}
                      layout="vertical"
                      ref={this.formSubCategoryRef}
                      name="basic"
                      initialValues={{ remember: true }}
                      onFinish={this.addSubCategoryOnFinish}
                      onFinishFailed={this.addSubCategoryonFinishFailed}
                    >
                      <Form.Item
                        name="category"
                        label="دپارتمان"
                        rules={[{ required: true, message: 'لطفا نام دپارتمان را انتخاب کنید.' }]}
                      >
                        <Select
                          placeholder=""
                          // onChange={this.onGenderChange}
                          allowClear
                        >
                          {allCategories ? (
                            allCategories.map((cat) => <Option value={cat.id}>{cat.name}</Option>)
                          ) : (
                            <Option value=""></Option>
                          )}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        label="کلاس"
                        name="subCategory"
                        rules={[{ required: true, message: 'لطفا نام کلاس را وارد کنید.' }]}
                      >
                        <Input />
                      </Form.Item>

                      <Form.Item>
                        <Button type="primary" htmlType="submit" loading={loading}>
                          ثبت
                        </Button>
                      </Form.Item>
                    </Form>
                  </div>
                </Col>
                <Col span={16} pull={8}>
                  <Form
                    ref={this.formStoreRef}
                    layout="vertical"
                    name="validate_other"
                    {...this.formItemLayout}
                    onFinish={this.onFinish}
                  >
                    <Form.Item
                      label="نام"
                      name="firstName"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا نام خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      label="نام خانوادگی"
                      name="lastName"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا نام خانوادگی خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      label="نام پدر"
                      name="fatherName"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا نام پدر خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      label="شماره شناسنامه"
                      name="idNumber"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا شماره شناسنامه خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <Form.Item
                      label="شماره گواهینامه"
                      name="certNumber"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا شماره گواهینامه خود را وارد کنید.',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>

                    <Form.Item
                      name="category"
                      label="دپارتمان"
                      rules={[{ required: true, message: 'لطفا نام دپارتمان را انتخاب کنید.' }]}
                    >
                      <Select placeholder="" onChange={this.onDepartmantChange} allowClear>
                        {allCategories ? (
                          allCategories.map((cat) => <Option value={cat.id}>{cat.name}</Option>)
                        ) : (
                          <Option value=""></Option>
                        )}
                      </Select>
                    </Form.Item>

                    <Form.Item
                      name="subCategory"
                      label="کلاس"
                      rules={[{ required: true, message: 'لطفا نام کلاس را انتخاب کنید.' }]}
                    >
                      <Select placeholder="" allowClear>
                        {allCategories ? (
                          allSubCategories.map((cat) => <Option value={cat.id}>{cat.name}</Option>)
                        ) : (
                          <Option value=""></Option>
                        )}
                      </Select>
                    </Form.Item>

                    <Form.Item
                      label="فایل گواهینامه"
                      rules={[
                        {
                          required: true,
                          message: 'لطفا شماره گواهینامه خود را وارد کنید.',
                        },
                      ]}
                    >
                      <input required={true} type="file" onChange={this.handleChange} />
                    </Form.Item>

                    <Form.Item>
                      <Button type="primary" htmlType="submit" loading={loading}>
                        ثبت روی بلاکچین
                      </Button>
                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            </Content>
          </Layout>
        </Layout>
      </>
    );
  }
}

export default Store;
