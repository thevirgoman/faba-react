import tronWeb from './tronWeb';

export default async function () {
  const newTronWeb = await tronWeb();

  let address = process.env.REACT_APP_CONTRACT_ADDRESS;
  address = newTronWeb.address.fromHex(address);

  return newTronWeb.contract().at(address);
}
