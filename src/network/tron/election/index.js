import getContract from '../core/getContract';

export const storeHash = async (hash) => {
  const contract = await getContract();
  const result = await contract.storeHash(hash).send();
  return result;
};
export const verifyHash = async (hash) => {
  const contract = await getContract();
  const result = await contract.verifyHash(hash).call();
  return result;
};
